import java.io._
import java.net._

object UdpServer {
  def main(args: Array[String]) {
    val SIZE = 1024
    val PORT = 58285

    val socket = new DatagramSocket(PORT)

    while (true) {
      val buffer = new Array[Byte](SIZE)
      val packet = new DatagramPacket(buffer, buffer.length)
      socket.receive(packet)
      val message = new String(packet.getData)
      val ipAddress = packet.getAddress().toString
      println("received from " + ipAddress + ": " + message)

      // respond
      socket.send(packet)
    }
  }
}