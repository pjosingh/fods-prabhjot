/**
  * Created by PRAB on 05.05.2017.
  */
abstract sealed class MsgType
case class Msg(message: String) extends MsgType
case class End() extends MsgType
