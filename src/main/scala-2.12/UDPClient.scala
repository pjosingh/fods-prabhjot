import java.io._
import java.net._
import com.twitter.chill._
import Console.{GREEN, RED, RESET, YELLOW_B, UNDERLINED}

object UdpClient {
  def main(args: Array[String]) {
    val SIZE = 512
    val PORT = 58285
    val address = ""
    val socket = new DatagramSocket(PORT)

    val kryo = ScalaKryoInstantiator.defaultPool

    val candidate = io.StdIn.readInt().ensuring(_ >= 1).ensuring(_ <= 2)
    candidate match {
      case 1 => sendMessage(new Msg(io.StdIn.readLine()))
      case 2 => sendMessage(new End())
    }

    val prime = (2 to candidate - 1).forall(candidate % _ != 0)

    if (prime)
      Console.println(s"${RESET}${GREEN}yes${RESET}")
    else
      Console.err.println(s"${RESET}${YELLOW_B}${RED}${UNDERLINED}NO!${RESET}")
    val packet = new DatagramPacket(new Array[Byte](SIZE), SIZE)
    packet.setAddress(new InetAddress())


    //    while (true) {
    //      val buffer = new Array[Byte](SIZE)
    //      val packet = new DatagramPacket(buffer, buffer.length)
    //      socket.receive(packet)
    //      val message = new String(packet.getData)
    //      val ipAddress = packet.getAddress().toString
    //      println("received from " + ipAddress + ": " + message)
    //
    //    }
  }

  def sendMessage =
}