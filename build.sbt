name := "FoDS Exercise 1"

version := "1.0"

scalaVersion := "2.12.2"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"
libraryDependencies += "com.twitter" %% "chill" % "0.9.2"
